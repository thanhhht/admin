<div class="left-menu">  
    <?php 
        $service = DB::table('service')->get();
    ?>
    @foreach($service as $sv)
    <div class="part">        
        <h3 class="part-title">{{$sv->ten_service}}<i class="fa fa-chevron-left"></i></h3>
        <div class="part-content">
            <?php  
                $congty = DB::table('thongtinkythuat')->where('service',$sv->id)
                                                      ->join('congty','thongtinkythuat.cty_id','=','congty.id')
                                                      ->select('thongtinkythuat.cty_id', 'ten_cty')->distinct()
                                                      ->get();                                    
            ?>
            @foreach($congty as $ct)
            <div class="list">
                <h4 class="list-title">{{$ct->ten_cty}}<i class="fa fa-chevron-left"></i></h4>
                <ul>
                    <?php  
                        $chinhanh = DB::table('chinhanh')
                        ->join('thongtinkythuat', 'chinhanh.id', '=', 'thongtinkythuat.cn_id')
                        ->where([['chinhanh.cty_id','=',$ct->cty_id],['thongtinkythuat.service','=',$sv->id]])
                        ->get();
                    ?>
                    @foreach($chinhanh as $cn)
                    <li data-thongtin = "{{$cn->id}}">{{$cn->ten}}</li>
                    @endforeach                      
                </ul>
            </div>  
            @endforeach
        </div>
    </div>
    @endforeach   
    <div class="part">
        <h3 class="part-title">Up link</h3>
    </div>
    <div class="part" id="dv-lienhe">
        <h3 class="part-title">Liên hệ</h3>
    </div>
</div>