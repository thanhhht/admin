$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function(){
	// ajax lấy thông tin kỹ thuật đổ ra
	$('.list li').click(function(event){
		$('.bottom-right-content').hide();
		$.ajax({
			type:"POST",
			url:"thongtin",
			data: {'id':$(this).data("thongtin")},	
			success: function(data){
				console.log(data);
				// $('.right-content').html(data);
				$(data).each(function (index, value) {
					$("#tenchinhanh").html(value.ten);
 					$("#ttkh").html('Thông tin khách hàng:');
 					$('#diachi').html('Địa chỉ: '+value.diachi);
 					$('#lienhe').html('Liên hệ: '+value.lienhe);	
 					$("#ttkt").html('Thông tin kỹ thuật:');

 					$("#service").html(value.ten_service);
 					$("#ip_sub").html('IP KH: ' + value.ip_kh + '<br/>'+'Subnet: ' + value.subnet);
 					$("#u_id").html(value.user_id);
 					$("#password").html(value.password);
 					$("#ip_pppoe").html(value.ip_pppoe);
 					$("#equipment").html(value.equipment);									
 					$("#hd").html('HD'+'<br/>'+value.hd);
 					$("#tt").html('TT'+'<br/>'+value.tt);
 					$("#hinh").attr("src",value.sodo);

 					if (value.mtt != null) 
 					{			
 						$("#mtt1").html('Sw/Port/SFP');
 						$("#mtt2").html('VLAN');
 						$("#mtt3").html('IP MGMT');

 						$("#sw_sort_sfp").html(value.sw_port_sfp);
 						$("#vlan").html(value.vlan);
 						$("#ip_mgmt").html(value.ip_mgmt);
 					}
 					else
 					{
 						$("#mtt1").html('User ID');
 						$("#mtt2").html('Password');
 						$("#mtt3").html('IP PPPoE');

 						$("#sw_sort_sfp").html(value.ol_user_id);
 						$("#vlan").html(value.ol_password);
 						$("#ip_mgmt").html(value.ol_ip_pppoe);
 					}
					
 					$('#table-thongtin').show();
 					$('#sodo').show()
 					
				});
			},
			error: function(){
				alert("Đã có lỗi xảy ra");
			}
		});
	});
});