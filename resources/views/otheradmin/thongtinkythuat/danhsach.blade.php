@extends('layout.adminlayout')
@section('content')
    <div class="right-content">
        <div style="margin-bottom:10px; color:gray; font-size:1.25em">
            <b style="font-size:2.5em; color:black" id="ds">Thông tin kỹ thuật</b> danh sách
            <span style="float:right">
                <a href="admin/thongtinkythuat/them">
                    <button class='btn-them mg-top'>Thêm</button>
                </a>
            </span>
        </div>
        <hr/>
        <table id="table" style="margin-top: 30px; width:100%;">
            <tr>
                @foreach($tencot as $tc)
                <th>{{$tc}}</th>
                @endforeach
                <th>Sửa/Xóa</th>
            </tr>

            @foreach($ds as $ct)
            <tr>               
                <td>{{$ct->id}}</td>
                <td>{{$ct->cn_id}}</td>
                <td>{{$ct->cty_id}}</td>
                <td>{{$ct->service}}</td>
                <td>{{$ct->main_line}}</td>
                <td>{{$ct->backup_line}}</td>
                <td>{{$ct->equipment}}</td>
                <td>{{$ct->sodo}}</td>
                <td>{{$ct->ip_kh}}</td>
                <td>{{$ct->subnet}}</td>
                <td>{{$ct->hd}}</td>
                <td>{{$ct->tt}}</td>
                <td>{{$ct->created_at}}</td>
                <td>{{$ct->updated_at}}</td>    
                <td>{{$ct->created_by}}</td>      
                <td>
                    <a href='admin/thongtinkythuat/sua/{{$ct->id}}'><button class="btn-sua" >Sửa</button></a>
                    <button class="btn-xoa" link='admin/thongtinkythuat/xoa/{{$ct->id}}'>Xóa</button>
                </td>   
            </tr>
            @endforeach
        </table>
    </div>

@endsection