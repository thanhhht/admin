@extends('layout.adminlayout')
@section('content')  
<div class="right-content">
        @if(session('thongbao'))
            <div style="background-color:red">
                {{session('thongbao')}}
            </div>
        @endif
        
        <form action="admin/thongtinkythuat/sua/{{$ttkt->id}}" method="POST" style="padding-left: 8px; padding-bottom: 50px;">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Thông tin kỹ thuật</b> chỉnh sửa
            </div>
            <hr/>
            <div class="form-group">
                <label>Chi nhánh:</label>
                <select name="chinhanh">
                    @foreach($chinhanh as $cn)
                    <option 
                        value="{{$cn->id}}"
                        @if($cn->id == $ttkt->cn_id)
                            {{"selected"}}
                        @endif
                    >{{$cn->ten}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Service:</label>
                <select name="service">
                    @foreach($service as $sv)
                    <option 
                        value="{{$sv->id}}"
                        @if($sv->id == $ttkt->service)
                            {{"selected"}}
                        @endif
                    >{{$sv->ten_service}}</option>
                    @endforeach
                </select>
            </div>
            <div style="display:flex; margin-top:40px;">
                <fieldset class="fieldset">
                    <legend>Main Line</legend>    
                                   
                    <div class="group-mtt">                       
                    </div>
                    
                    <div class="group-otherline">                        
                    </div>
                    
                </fieldset>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <fieldset class="fieldset backup-fieldset" style="padding-top:0px;">
                    <legend>Backup Line</legend>
                    <div class="form-group">
                        <label>UserID:</label>
                        <input type='text' name='user_id' value="{{$backupline->user_id}}"/>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type='text' name='password' value="{{$backupline->password}}"/>
                    </div>
                    <div class="form-group">
                        <label>IP PPPOE:</label>
                        <input type='text' name='ip_pppoe' value="{{$backupline->ip_pppoe}}"/>
                    </div>
                </fieldset>
            </div>
            
            <div class="form-group">
                <label>Equipment:</label>
                <textarea rows="5" name="equipment">{{$ttkt->equipment}}</textarea>
            </div>

            <div class="form-group">
                <label>Sơ đồ:</label>
                <input type="text" name="sodo" value="{{$ttkt->sodo}}"/>
            </div>

            <div class="form-group">
                <label>IP Khách hàng:</label>
                <input type="text" name="ip_kh" value="{{$ttkt->ip_kh}}"/>
            </div>

            <div class="form-group">
                <label>Subnet:</label>
                <input type="text" name="subnet" value="{{$ttkt->subnet}}"/>
            </div>

            <div class="form-group">
                <label>HD:</label>
                <input type="text" name="hd" value="{{$ttkt->hd}}"/>
            </div>
            
            <div class="form-group">
                <label>TT:</label>
                <input type="text" name="tt" value="{{$ttkt->tt}}"/>
            </div>
            <input class="mtt-other" type="text" name="mtt_other" style="display:none"/>
            <div>
                <button type="submit" class="btn-sua">Lưu thay đổi</button>
            </div>
        </form>   
               
        @if($mtt != null)
            <script>
                $('.mtt-other').val('mtt'); 
                $('.group-mtt').html('');
                var div1=$('<div class="form-group"/>');
                var div2=$('<div class="form-group"/>');
                var div3=$('<div class="form-group"/>');
                $('<label/>').html('SW/PORT/SFP:').appendTo(div1);
                $("<input type='text' name='sw_port_sfp' value='{{$mtt->sw_port_sfp}}'/>").appendTo(div1);
                $('<label/>').html('VLAN:').appendTo(div2);
                $("<input type='text' name='vlan' value='{{$mtt->vlan}}'/>").appendTo(div2);
                $('<label/>').html('IP MGMT:').appendTo(div3);
                $("<input type='text' name='ip_mgmt' value='{{$mtt->ip_mgmt}}'/>").appendTo(div3);
                div1.appendTo('.group-mtt');
                div2.appendTo('.group-mtt');
                div3.appendTo('.group-mtt');
            </script>
        @else
            <script>           
                $('.mtt-other').val('other');    
                $('.group-otherline').html('');
                var div1=$('<div class="form-group"/>');
                var div2=$('<div class="form-group"/>');
                var div3=$('<div class="form-group"/>');
                $('<label/>').html('UserID:').appendTo(div1);
                $("<input type='text' name='ol_user_id' value='{{$otherline->ol_user_id}}'/>").appendTo(div1);
                $('<label/>').html('Password:').appendTo(div2);
                $("<input type='text' name='ol_password' value='{{$otherline->ol_password}}'/>").appendTo(div2);
                $('<label/>').html('IP PPPOE:').appendTo(div3);
                $("<input type='text' name='ol_ip_pppoe' value='{{$otherline->ol_ip_pppoe}}'/>").appendTo(div3);
                div1.appendTo('.group-otherline');
                div2.appendTo('.group-otherline');
                div3.appendTo('.group-otherline');
            </script>
        @endif
    </div>
@endsection