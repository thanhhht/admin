<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mtt extends Model
{
    protected $table = "mtt";
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
