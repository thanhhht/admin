<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class LoginController extends Controller
{
    // Trả về trang đăng nhập
    function getLogin(){
        return view('login.dangnhap');
    }

    // Xử lý đăng nhập
    function postLogin(Request $rq){
        if(Auth::attempt(['user_name'=>$rq->user_name,'password'=>$rq->password])){
            return redirect('admin');
        }
        else{                   
            return redirect('login')->with('thongbao','Đăng nhập thất bại!');
        }
    }

    // Xử lý đăng xuất
    function getLogout(){
        Auth::logout();
        return redirect('login')->with('thongbao','Đã log out');
    }
}
