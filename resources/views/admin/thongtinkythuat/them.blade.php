@extends('layout.adminlayout')
@section('content')  
<div class="right-content">
        @if(session('thongbao'))
            <div class="alert-danger">
                {{session('thongbao')}}
            </div>
        @endif     
        <form action="admin/thongtinkythuat/them" enctype='multipart/form-data' method="POST" style="padding-left: 8px; padding-bottom: 50px;">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Thông tin kỹ thuật</b> thêm
            </div>
            <hr/>
            <div class="form-group">
                <label>Chi nhánh:</label>
                <select name="chinhanh">
                    @foreach($chinhanh as $cn)
                    <option value="{{$cn->id}}">{{$cn->ten}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Service:</label>
                <select name="service">
                    @foreach($service as $sv)
                    <option value="{{$sv->id}}">{{$sv->ten_service}}</option>
                    @endforeach
                </select>
            </div>

            <div style="display:flex; margin-top:40px;">
                <fieldset class="fieldset">
                    <legend>Main Line</legend>
                    <div id="radio-group" style="margin-top:15px;">
                        <input type="radio" name="mainline-radio" required="true" value='mtt'/> MTT&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="mainline-radio" required="true" value='otherline'/> OtherLine
                    </div>

                    <div class="group-mtt">
                        <div class="form-group">
                            <label>SW/PORT/SFP:</label>
                            <input type='text'  name='sw_port_sfp'/>
                        </div>
                        <div class="form-group">
                            <label>VLAN:</label>
                            <input type='text'  name='vlan'/>
                        </div>
                        <div class="form-group">
                            <label>IP MGMT:</label>
                            <input type='text'  name='ip_mgmt'/>
                        </div>
                    </div>

                    <div class="group-otherline">
                        <div class="form-group">
                            <label>UserID:</label>
                            <input type='text' name='ol_user_id'/>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input type='text' name='ol_password'/>
                        </div>
                        <div class="form-group">
                            <label>IP PPPOE:</label>
                            <input type='text' name='ol_ip_pppoe'/>
                        </div>
                    </div>
                </fieldset>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <fieldset class="fieldset backup-fieldset">
                    <legend>Backup Line</legend>
                    <div class="form-group">
                        <label>UserID:</label>
                        <input type='text' name='user_id'/>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type='text' name='password'/>
                    </div>
                    <div class="form-group">
                        <label>IP PPPOE:</label>
                        <input type='text' name='ip_pppoe'/>
                    </div>
                </fieldset>
            </div>
            
            <div class="form-group">
                <label>Equipment:</label>
                <textarea rows="5" required="true" name="equipment"></textarea>
            </div>

            <div class="form-group">
                <label>Sơ đồ:</label>
                <input type="text" name="sodo"/>
            </div>

            <div class="form-group">
                <label>IP Khách hàng:</label>
                <input type="text" required="true" name="ip_kh"/>
            </div>

            <div class="form-group">
                <label>Subnet:</label>
                <input type="text" required="true" name="subnet"/>
            </div>

            <div class="form-group">
                <label>HD:</label>
                <input type="text" required="true" name="hd"/>
            </div>
            
            <div class="form-group">
                <label>TT:</label>
                <input type="text" required="true" name="tt"/>
            </div>
            <input class="mtt-other" type="text" name="mtt_other" style="display:none"/>
            <div>
                <button type="submit" class="btn-them">Thêm</button>
            </div>
        </form>
        <script>
            $('.group-mtt').hide();
            $('.group-otherline').hide();
            $('#radio-group').on('change', function() {
                if($('input[name=mainline-radio]:checked', '#radio-group').val() == 'mtt'){
                    $('.group-otherline').hide();
                    $('.group-mtt').show();
                    $('.mtt-other').val('mtt');                   
                }
                else if($('input[name=mainline-radio]:checked', '#radio-group').val() == 'otherline')
                {
                    $('.group-mtt').hide();
                    $('.group-otherline').show();
                    $('.mtt-other').val('other');
                }
            });
              
        </script>
    </div>
@endsection