<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backupline extends Model
{
    protected $table = "backup_line";
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
