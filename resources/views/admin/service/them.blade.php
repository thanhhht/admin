@extends('layout.adminlayout')
@section('content')
    <div class="right-content"> 
        <form action="admin/service/them" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Service</b> thêm
            </div>
            <hr/>
            <div class="form-group">
                <label>Tên dịch vụ:</label>
                <input type='text' name="service"/>
            </div>
            <div>
                <button type="submit" class="btn-them">Add</button>
            </div>
        </form>
        
    </div>
@endsection