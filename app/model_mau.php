<?php
namespace App\common\web;
use Illuminate\Database\Eloquent\Model;
use App\common\web\GroupUser;
use Auth;
use File;
// use App\common\Main as Model;

class CardList extends Model
{
    protected $table	                     = 'card_list';
    private $data_path                       = 'storage/data/';
	//const TABLE = 'groups';
    public function __construct()
    {	
    	
    }
    protected $dateFormat = 'U'; 

    private static function getCardOrderArray(){
       
    }

    public function Table(){
        return $this->belongsTo('App\common\web\Table','id_table','id')
                    ->select([
                        'id',
                        'name',
                        'id_group',
                        'slug',
                    ])
                    ->orderBy('id','ASC');
    }

    public function getTableName(){
        return $this->table;
    }

    public function Card(){
        //sort Card theo thứ tự json khi lấy ra detail table
        $order_json_path              = 'storage/data/'.'group-'.$this->Table->id_group.'/table-'.$this->id_table.'/card-list-'.$this->id.'/card_order.json';
        $order_array                  = json_decode(file_get_contents($order_json_path),true);
        asort($order_array);  //sắp xếp order array

        $order_raw                    = self::orderByRawString($order_array);
        return $this->hasMany('App\common\web\Card','id_card_list','id')
                    ->select([
                        'id',
                        'id_card_list',
                        'title',
                        'content',
                        'slug',
                        'order',
                        'due_date',
                        'label',
                        'status',
                    ])
                    ->orderByRaw($order_raw);
    }  

    public static function newCardList($req){ //tao moi viet 1 ham ne
        try{
            if(empty($req) || $req == null){
                return false;
            }
            $data = new \stdClass();
            $data->name                 = $req->name;
            $next_id                    = self::max('id')+1;
            $data->slug                 = replace($req->name).'-'.$next_id;
            $data->id_table             = $req->id_table;
            $data->order                = self::select('id')
            ->where('id_table',$req->id_table)
            ->count()+1;
            $fields                     = array();
            foreach ($data as $field=>$item):
            array_push($fields, $field);  //lấy các trường để select
        endforeach;
        $model                      = new self();
        foreach ($data as $field=>$item):
            $model->{$field}  = $item;
        endforeach;
        if($model->save()){
            return $model;
        }
        else{
            return false;
        }
    }
        catch (\Exception $e){
            print_r($e->getMessage());
        }
    }

    public static function allCardListInTable($id_table,$order_array=null){ //thêm phần sort theo json
        $order_raw =  self::orderByRawString($order_array);
        return self::select([
                           'id',
                           'name',
                           'id_table'
                       ])
                        ->where('id_table',$id_table)
                        ->orderByRaw($order_raw)
                        ->get();
    }

    public static function orderByRawString($order_array){ //ghép các thứ tự id thành chuỗi để vào hàm order
        if($order_array != null){
            $order_raw = "FIELD(id";
            foreach ($order_array as $key => $value) {
                $order_raw .= ", "."'".$key."'";
            }
            $order_raw .= ")";
            return $order_raw;
        }
        return "id";
    }

    public static function deleteAllCardInCardList($id_card_list){ //xoa 1 ham
        try {
            $cards = Card::allCardInCardList($id_card_list);
            foreach ($cards as $card) {
                $card->delete();
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    public static function getCardListById($id_card_list){ //get by id 1 ham
        return self::select([
                            'id',
                            'name',
                            'slug',
                            'id_table',
                            'order',
                            ])
                    ->where('id',$id_card_list)
                    ->first();
    }

    public static function getAllCardInCardList($id_card_list){ //hieu ko ? hieu mà h viết z đi sau nay update để chạy dc đã Lân nó cần gấp v maf thoi gio ban qua m lam tiep di, lam chung conflict lawm, khi nao be tac gi t giup ok, pussh len tan thanh thuong xuyen nha chi commit vs push lai vs um commit xong push
         return self::select([
                            'id',
                            'id_table',
                            ])
                    ->where('id',$id_card_list)
                    ->first()->Card;
    }

    public static function updateCardList($req){
        try {
            $model              = CardList::getCardListById($req->id_card_list);
            if (isset($req->name)){
                $model->name        = $req->name;
            }
            if(isset($req->id_table)){
                $model->id_table        = $req->id_table;
            }
            if($model->save()){
                return $model;
            }
            else{
                return false;
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    public static function deleteCardList($id){
      try {
        $model = self::getCardListById($id);
        if ($model!=null){
            File::deleteDirectory('storage/data/group-'.$model->Table->id_group.'/table-'.$model->Table->id.'/card-list-'.$model->id);
            $model->delete();
            return true;
        }
        else{
            return false;
        }
      } 
      catch (Exception $e) {
            echo($e->getMessage());
      }
    }

    public static function copyCardList($id_card_list, $id_table_destination){
        try {
            $card_list                           = self::getCardListById($id_card_list);
            $new_card_list                       = $card_list->replicate();
            $new_card_list->id_table             = $id_table_destination;
            $new_card_list->slug                 = replace($new_card_list->name).'-'.(self::max('id')+1);
            if($new_card_list->save()){
                return $new_card_list;
            }
            else{
                return false;
            }
        } catch (\Exception $e) {
            echo($e->getMessage());
        }
    }
}
