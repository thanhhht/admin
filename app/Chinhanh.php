<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chinhanh extends Model
{
    protected $table = "chinhanh";

    // Hàm lấy tên các cột của bảng
    public static function getTableColumns() {
        $chinhanh = new Chinhanh;
        return $chinhanh->getConnection()->getSchemaBuilder()->getColumnListing($chinhanh->getTable());
    }

    // Hàm thêm mới item
    public static function addChiNhanh($rq){
        $chinhanh = new Chinhanh;
        $chinhanh->ten = $rq->tenchinhanh;
        $chinhanh->cty_id = $rq->congty;
        $chinhanh->diachi = $rq->diachi;
        $chinhanh->lienhe = $rq->lienhe;
        $chinhanh->save();
    }

    // Hàm chỉnh sửa item
    public static function editChiNhanh($rq, $id){
        $chinhanh = Chinhanh::find($id);
        $chinhanh->ten = $rq->tenchinhanh;
        $chinhanh->cty_id = $rq->congty;
        $chinhanh->diachi = $rq->diachi;
        $chinhanh->lienhe = $rq->lienhe;
        $chinhanh->save();
    }

    // Hàm xóa
    public static function deleteChiNhanh($id){
        $chinhanh = Chinhanh::find($id);
        $chinhanh->delete();
    }
}
