<div class="left-menu">   
    <div class="part">        
        <a href="admin/congty/danhsach"><h3 class="table-name" >Công ty</h3></a>        
    </div>  
    <div class="part">        
        <a href="admin/chinhanh/danhsach"><h3 class="table-name" >Chi nhánh</h3></a>        
    </div>
    <div class="part">        
        <a href="admin/service/danhsach"><h3 class="table-name" >Service</h3></a>        
    </div>
    <div class="part">        
        <a href="admin/thongtinkythuat/danhsach"><h3 class="table-name">Thông tin kỹ thuật</h3></a>        
    </div>
    @if(Auth::user()->role == 1)
    <div class="part">        
        <a href="admin/user/danhsach"><h3 class="table-name">User</h3></a>        
    </div>
    @endif
</div>