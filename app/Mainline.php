<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainline extends Model
{
    protected $table = "main_line";

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
