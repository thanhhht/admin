@extends('layout.adminlayout')
@section('content')  
<div class="right-content">
        @if(session('thongbao'))
            <div style="background-color:red">
                {{session('thongbao')}}
            </div>
        @endif
        
        <form action="admin/service/sua/{{$service->id}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Service</b> chỉnh sửa
            </div>
            <hr/>
            <div class="form-group">
                <label>Tên dịch vụ:</label>
                <input type='text' name="service" value="{{$service->ten_service}}"/>
            </div>
            <div>
                <button type="submit" class="btn-sua">Lưu thay đổi</button>
            </div>
        </form>
        
    </div>
@endsection