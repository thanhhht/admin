@extends('layout.adminlayout')
@section('content')
    <div class="right-content">
        @if(session('thongbao'))
            <div class="alert-success">
                {{session('thongbao')}}
            </div>
        @endif
        <div style="margin-bottom:10px; color:gray; font-size:1.25em">
            <b style="font-size:2.5em; color:black" id="ds">Service</b> danh sách
            <span style="float:right"><a href="admin/service/them"><button class="btn-them mg-top">Thêm</button></a></span>
        </div>
        <hr/>
        <table id="table" style="margin-top: 30px;">
            <tr>
                @foreach($tencot as $tc)
                <th>{{$tc}}</th>
                @endforeach
                <th>Sửa/Xóa</th>
            </tr>

            @foreach($ds as $ct)
            <tr>               
                <td>{{$ct->id}}</td>
                <td>{{$ct->ten_service}}</td>
                <td>{{$ct->created_at}}</td>
                <td>{{$ct->updated_at}}</td>
                <td>
                    <a href="admin/service/sua/{{$ct->id}}"><button class="btn-sua">Sửa</button></a>
                    <button class="btn-xoa" link="admin/service/xoa/{{$ct->id}}">Xóa</button>
                </td>               
            </tr>
            @endforeach
        </table>
    </div>
@endsection