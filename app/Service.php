<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "service";

    public static function getTableColumns() {
        $service = new Service;
        return $service->getConnection()->getSchemaBuilder()->getColumnListing($service->getTable());
    }

    public static function addService($rq){
        $service = new Service;
        $service->ten_service = $rq->service;
        $service->save();
    }

    public static function editService($rq, $id){
        $service = Service::find($id);
        $service->ten_service = $rq->service;
        $service->save();
    }

    public static function deleteService($id){
        $service = Service::find($id);
        $service->delete();
    }
}
