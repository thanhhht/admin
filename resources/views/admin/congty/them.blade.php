@extends('layout.adminlayout')
@section('content')
    <div class="right-content">      
        <form action="admin/congty/them" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em"><b style="font-size:2.5em; color:black" id="ds">Công ty</b> thêm</div>
            <hr/>
            <div class="form-group">
                <label>Tên công ty:</label>
                <input type='text' name="tencongty"/>
            </div>
            <div>
                <button type="submit" class="btn-them">Thêm</button>
            </div>
        </form>        
    </div>
@endsection