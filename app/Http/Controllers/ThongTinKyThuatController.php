<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thongtinkythuat;
use App\Chinhanh;
use App\Service;
use App\Mainline;
use App\Backupline;
use App\Mtt;
use App\Otherline;
use Auth;

class ThongTinKyThuatController extends Controller
{
    function getDanhSach(){
        $tencot = Thongtinkythuat::getTableColumns();
        $ds = Thongtinkythuat::all();
        if (Auth::user()->role == 2){
            $otherds = Thongtinkythuat::getOtherTTKT();
            return view('otheradmin.thongtinkythuat.danhsach',['ds'=>$otherds, 'tencot'=>$tencot]);            
        }
        return view('admin.thongtinkythuat.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
    }

    function getThem(){       
        $chinhanh = Chinhanh::all();
        $service = Service::all();
        $main_line = Mainline::all();
        $backup_line = Backupline::all();
        return view('admin.thongtinkythuat.them',['chinhanh'=>$chinhanh, 'service'=>$service, 
                                                 'main_line'=>$main_line, 'backup_line'=>$backup_line]);
    }

    function postThem(Request $rq){  
        if(Thongtinkythuat::checkExist($rq)){
            Thongtinkythuat::addThongTinKyThuat($rq);
            return redirect('admin/thongtinkythuat/danhsach')->with('thongbao','Thêm thành công!');
        }   
        else{
            return redirect('admin/thongtinkythuat/them')->with('thongbao','Chi nhánh này đã được đăng ký dịch vụ này rồi!');
        }
    }

    function getSua($id){
        $ttkt = Thongtinkythuat::find($id);
        $chinhanh = Chinhanh::all();
        $service = Service::all();
        $main_line = Mainline::find($ttkt->main_line);
        $backup_line = Backupline::find($ttkt->backup_line);       
        $mtt = Mtt::find($main_line->mtt);           
        $otherline = Otherline::find($main_line->other_line);

        return view('admin.thongtinkythuat.sua',[
                                                    'ttkt'=>$ttkt,
                                                    'chinhanh'=>$chinhanh, 
                                                    'service'=>$service,
                                                    'otherline'=>$otherline,
                                                    'mtt'=>$mtt,
                                                    'backupline'=>$backup_line
                                                ]);       
    }

    function postSua(Request $rq, $id){
        Thongtinkythuat::editThongTinKyThuat($rq, $id);
        return redirect('admin/thongtinkythuat/danhsach')->with('thongbao','Sửa thành công');
    }

    function getXoa($id){
        Thongtinkythuat::deleteThongTinKyThuat($id);     
    }
}
