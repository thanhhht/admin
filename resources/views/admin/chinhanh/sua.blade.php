@extends('layout.adminlayout')
@section('content')  
<div class="right-content">
        @if(session('thongbao'))
            <div style="background-color:red">
                {{session('thongbao')}}
            </div>
        @endif
        
        <form action="admin/chinhanh/sua/{{$chinhanh->id}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Chi nhánh</b> chỉnh sửa
            </div>
            <hr/>
            <div class="form-group">
                <label>Tên chi nhánh:</label>
                <input type='text' name="tenchinhanh" value="{{$chinhanh->ten}}"/>
            </div>

            <div class="form-group">
                <label>Công ty:</label>
                <select name="congty">
                    @foreach($congty as $ct)
                    <option 
                        value="{{$ct->id}}"
                        @if($ct->id == $chinhanh->cty_id)
                            {{"selected"}}
                        @endif
                    >{{$ct->ten_cty}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Địa chỉ:</label>
                <input type='text' name="diachi" value="{{$chinhanh->diachi}}"/>
            </div>
            <div class="form-group">
                <label>Liên hệ:</label>
                <input type='text' name="lienhe" value="{{$chinhanh->lienhe}}"/>
            </div>
            <div>
                <button type="submit" class="btn-sua">Lưu thay đổi</button>
            </div>
        </form>
        
    </div>
@endsection