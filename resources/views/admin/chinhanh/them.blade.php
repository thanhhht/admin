@extends('layout.adminlayout')
@section('content')
    <div class="right-content">      
        <form action="admin/chinhanh/them" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em">
                <b style="font-size:2.5em; color:black" id="ds">Chi nhánh</b> thêm
            </div>
            <hr/>
            <div class="form-group">
                <label>Tên chi nhánh:</label>
                <input type='text' name="tenchinhanh" />
            </div>

            <div class="form-group">
                <label>Công ty:</label>
                <select name="congty">
                    @foreach($congty as $ct)
                    <option 
                        value="{{$ct->id}}"                        
                    >{{$ct->ten_cty}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Địa chỉ:</label>
                <textarea rows="5" name="diachi"></textarea>
            </div>
            <div class="form-group">
                <label>Liên hệ:</label>
                <textarea rows="5" name="lienhe"></textarea>
            </div>
            <div>
                <button type="submit" class="btn-them">Thêm</button>
            </div>
        </form>
        
    </div>
@endsection