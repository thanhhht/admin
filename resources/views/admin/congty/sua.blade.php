@extends('layout.adminlayout')
@section('content')  
<div class="right-content">
        @if(session('thongbao'))
            <div style="background-color:red">
                {{session('thongbao')}}
            </div>
        @endif
        
        <form action="admin/congty/sua/{{$congty->id}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div style="margin-bottom:10px; color:gray; font-size:1.25em"><b style="font-size:2.5em; color:black" id="ds">Công ty</b> chỉnh sửa</div>
            <hr/>
            <div class="form-group">
                <label>Tên công ty:</label>
                <input type='text' name="tencongty" value="{{$congty->ten_cty}}"/>
            </div>
            <div>
                <button type="submit" class="btn-sua">Lưu thay đổi</button>
            </div>
        </form>
        
    </div>
@endsection