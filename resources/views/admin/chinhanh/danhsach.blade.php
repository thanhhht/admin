@extends('layout.adminlayout')
@section('content')
    <div class="right-content">   
        <div style="margin-bottom:10px; color:gray; font-size:1.25em">
            <b style="font-size:2.5em; color:black" id="ds">Chi nhánh</b> danh sách
            <span style="float:right"><a href="admin/chinhanh/them"><button class="btn-them mg-top">Thêm</button></a></span>
        </div>
        <hr/>
        <table id="table" style="margin-top: 30px;width:100%;">
            <tr>
                @foreach($tencot as $tc)
                <th>{{$tc}}</th>
                @endforeach
                <th>Sửa/Xóa</th>
            </tr>

            @foreach($ds as $ct)
            <tr>               
                <td>{{$ct->id}}</td>
                <td>{{$ct->ten}}</td>
                <td>{{$ct->cty_id}}</td>
                <td>{{$ct->diachi}}</td>
                <td>{{$ct->lienhe}}</td>
                <td>{{$ct->created_at}}</td>
                <td>{{$ct->updated_at}}</td>
                <td style="width:15%">
                    <a href='admin/chinhanh/sua/{{$ct->id}}'><button class="btn-sua" >Sửa</button></a>
                    <button class="btn-xoa" link='admin/chinhanh/xoa/{{$ct->id}}'>Xóa</button>
                </td>               
            </tr>
            @endforeach
        </table>
    </div>
@endsection