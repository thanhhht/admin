<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Auth;

class ServiceController extends Controller
{
    function getDanhSach(){
        $tencot = new Service;
        $tencot = $tencot->getTableColumns();
        $ds = Service::all();
        if (Auth::user()->role == 2){
            return view('otheradmin.service.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
        }
        return view('admin.service.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
    }

    function getOtherDanhSach(){
        $tencot = new Service;
        $tencot = $tencot->getTableColumns();
        $ds = Service::all();
        return view('otheradmin.service.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
    }

    function getThem(){
        return view('admin.service.them');
    }

    function postThem(Request $rq){
        Service::addService($rq);
        return redirect('admin/service/danhsach')->with('thongbao','Thêm thành công!');
    }

    function getSua($id){
        $service = Service::find($id);
        return view('admin.service.sua',['service'=>$service]);
    }

    function postSua(Request $rq, $id){
        Service::editService($rq, $id);
        return redirect('admin/service/danhsach')->with('thongbao','Sửa thành công!');
    }

    function getXoa($id){
        Service::deleteService($id);
    }
}
