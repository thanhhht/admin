<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Congty;
use Illuminate\Support\Collection;
use Auth;

class CongTyController extends Controller
{
    // Lấy tất cả item trong bảng đổ ra
    function getDanhSach(){
        $tencot = Congty::getTableColumns();
        $ds = Congty::all();
        if (Auth::user()->role == 2){
            return view('otheradmin.congty.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
        }
        return view('admin.congty.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
    }
   
    // Trả về view thêm
    function getThem(){
        return view('admin.congty.them');
    }

    // Xử lý thêm mới
    function postThem(Request $rq){
        Congty::addCongTy($rq);
        return redirect('admin/congty/danhsach')->with('thongbao','Thêm thành công!');
    }

    // Trả về view sửa
    function getSua($id){
        $congty = Congty::find($id);
        return view('admin.congty.sua',['congty'=>$congty]);
    }

    // Xử lý sửa
    function postSua(Request $rq, $id){        
        Congty::editCongTy($rq, $id);              
        return redirect('admin/congty/danhsach')->with('thongbao','Sửa thành công!');
    }

    // Xóa
    function getXoa($id){
        Congty::deleteCongTy($id);
    }

}
