<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chinhanh;
use App\Congty;
use Auth;

class ChiNhanhController extends Controller
{
    // Lấy tất cả item trong bảng đổ ra
    function getDanhSach(){
        $tencot = Chinhanh::getTableColumns();
        $ds = Chinhanh::all();
        if (Auth::user()->role == 2){
            return view('otheradmin.chinhanh.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
        }
        return view('admin.chinhanh.danhsach',['ds'=>$ds, 'tencot'=>$tencot]);
    }

    // Trả về view thêm
    function getThem(){
        $dscongty = Congty::all();
        return view('admin.chinhanh.them',['congty'=>$dscongty]);
    }

    // Xử lý thêm mới
    function postThem(Request $rq){
        Chinhanh::addChiNhanh($rq);
        return redirect('admin/chinhanh/danhsach')->with('thongbao','Thêm thành công!');
    }

    // Trả về view sửa
    function getSua($id){
        $chinhanh = Chinhanh::find($id);
        $congty = Congty::all();
        return view('admin.chinhanh.sua',['chinhanh'=>$chinhanh,'congty'=>$congty]);
    }

    // Xử lý sửa
    function postSua(Request $rq, $id){
        Chinhanh::editChiNhanh($rq, $id);
        return redirect('admin/chinhanh/danhsach')->with('thongbao','Sửa thành công!');
    }

    // Xóa
    function getXoa($id){
        Chinhanh::deleteChiNhanh($id);
    }    
}
