@extends('layout.adminlayout')
@section('content')
    <div class="right-content">
        <div style="margin-bottom:10px; color:gray; font-size:1.25em">
            <b style="font-size:2.5em; color:black" id="ds">Service</b> danh sách
        </div>
        <hr/>
        <table id="table" style="margin-top: 30px;">
            <tr>
                @foreach($tencot as $tc)
                <th>{{$tc}}</th>
                @endforeach
                
            </tr>

            @foreach($ds as $ct)
            <tr>               
                <td>{{$ct->id}}</td>
                <td>{{$ct->ten_service}}</td>
                <td>{{$ct->created_at}}</td>
                <td>{{$ct->updated_at}}</td>
                             
            </tr>
            @endforeach
        </table>
    </div>
@endsection