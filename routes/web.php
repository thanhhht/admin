<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('index', 'HomeController@index');

Route::get('admin', 'HomeController@admin')->middleware('checkLogin');
Route::get('otheradmin', 'HomeController@otheradmin');

Route::post('thongtin','HomeController@getId');

Route::group(['prefix'=>'admin','middleware'=>'checkLogin'],function(){
    Route::group(['prefix'=>'congty'],function(){
        Route::get('danhsach','CongTyController@getDanhSach');       
        Route::get('sua/{id}','CongTyController@getSua');
        Route::post('sua/{id}','CongTyController@postSua');
        Route::get('them','CongTyController@getThem');
        Route::post('them','CongTyController@postThem');
        Route::post('xoa/{id}','CongTyController@getXoa');
    });

    Route::group(['prefix'=>'chinhanh'],function(){
        Route::get('danhsach','ChiNhanhController@getDanhSach');        
        Route::get('sua/{id}','ChiNhanhController@getSua');
        Route::post('sua/{id}','ChiNhanhController@postSua');
        Route::get('them','ChiNhanhController@getThem');
        Route::post('them','ChiNhanhController@postThem');
        Route::post('xoa/{id}','ChiNhanhController@getXoa');
    });

    Route::group(['prefix'=>'service'],function(){
        Route::get('danhsach','ServiceController@getDanhSach');        
        Route::get('sua/{id}','ServiceController@getSua');
        Route::post('sua/{id}','ServiceController@postSua');
        Route::get('them','ServiceController@getThem');
        Route::post('them','ServiceController@postThem');
        Route::post('xoa/{id}','ServiceController@getXoa');
    }); 

    Route::group(['prefix'=>'thongtinkythuat'],function(){
        Route::get('danhsach','ThongTinKyThuatController@getDanhSach');       
        Route::get('them','ThongTinKyThuatController@getThem');
        Route::post('them','ThongTinKyThuatController@postThem');
        Route::get('sua/{id}','ThongTinKyThuatController@getSua');
        Route::post('sua/{id}','ThongTinKyThuatController@postSua');
        Route::post('xoa/{id}','ThongTinKyThuatController@getXoa');
    });

    Route::group(['prefix'=>'user'],function(){
        Route::get('danhsach','UserController@getDanhSach');       
        Route::get('sua/{id}','UserController@getSua');
        Route::post('sua/{id}','UserController@postSua');
        Route::get('them','UserController@getThem');
        Route::post('them','UserController@postThem');
        Route::post('xoa/{id}','UserController@getXoa');
    });
});

Route::get('login','LoginController@getLogin');
Route::post('login','LoginController@postLogin');
Route::get('logout','LoginController@getLogout');



