<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title>Đăng nhập</title>
    <link rel="stylesheet" href="{{asset('css/1.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>
<body>
    @if(session('thongbao'))
        <div class="alert-danger" style="margin:auto; width:35%; margin-top: 50px;">
            {{session('thongbao')}}
        </div>
    @endif
    <form class="form-login" action="login" method="POST">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div style="background-color:#2c3e50; text-align:center; padding:12px; color: white; font-size:1.5em;"><h3>Đăng nhập</h3></div>
        <div style="padding:10px 20px;">
            <div class="form-group">
                <label>Username:</label>
                <input type='text' name="user_name" style="width:100%"/>
            </div>
            <div class="form-group">
                <label>Password:</label>
                <input type='password' name="password" style="width:100%"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn-them" style="margin-top:20px; padding:15px">Đăng nhập</button>
            </div>
        </div>
    </form>
</body>
</html>