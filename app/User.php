<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_name', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function checkExist($rq){
        $user = User::where('user_name', $rq->user_name)->first();
        if($user != null){
            return false;
        }
        return true;
    }

    public static function addUser($rq){
        $user = new User;
        $user->name = $rq->name;
        $user->user_name = $rq->user_name;
        $user->password = Hash::make($rq->password);
        $user->role = $rq->role;
        $user->save();
    }

    public static function editUser($rq, $id){
        $user = User::find($id);

        $user->name = $rq->name;
        $user->user_name = $rq->user_name;
        $user->password = Hash::make($rq->password);
        $user->role = $rq->role;
        $user->save();
    }

    public static function deleteUser($id){
        $user = User::find($id);
        $user->delete();
    }
    
}
