<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin</title>
    <base href="{{asset('')}}">
    <script src="{{asset('/js/jquery-1.12.4.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/1.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/jquery-confirm-css/jquery-confirm.min.css')}}" />
    
    
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">

</head>
<body>
    <div class="big-text">
        <a href="index"><img src="{{asset('images/logo.png')}}" alt="" style="float: left;"></a>
        <span>QUẢN LÝ KHÁCH HÀNG</span>
        
        <span style="float:right; font-size:0.4em; margin-right:30px; margin-top:15px">{{Auth::user()->name}}&nbsp;&nbsp;&nbsp;<a href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a></span>
        
    </div>
    <div class="content">
        @include('partial.adminleft')
        @yield('content')
    </div>
    <script src="{{asset('js/1.js')}}"></script>
    <script src="{{asset('js/ajax.js')}}"></script>
    <script src="{{asset('js/jquery-confirm-js/jquery-confirm.min.js')}}"></script>   
    <script src="{{asset('js/delete-confirm.js')}}"></script>
</body>
</html>