<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Congty;

use DB;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
	// Trả về trang xem thông tin kỹ thuật khách hàng
    function index(){
    	return view('page.index');
	}
	// Trả về trang admin quản lý các bảng
	function admin(){	
		return view('admin.admin');
	}
	
	// Lấy id từng service để đổ ra menu 
    function getId(Request $rq){
    	$mtt = DB::table('thongtinkythuat')  
    		   ->where('thongtinkythuat.id',$rq->id)  	   
    		   ->join('main_line','thongtinkythuat.main_line','=','main_line.id')
    		   ->select('main_line.mtt', 'main_line.other_line')   		   
    		   ->get();

    	if ($mtt[0]->mtt != null) {
    		$thongtin = DB::table('thongtinkythuat')
    				->where('thongtinkythuat.id',$rq->id)
    				->join('chinhanh','thongtinkythuat.cn_id','=','chinhanh.id')
    				->join('service','thongtinkythuat.service','=','service.id')
    				->join('main_line','thongtinkythuat.main_line','=','main_line.id')
    				->join('mtt', 'main_line.mtt', '=', 'mtt.id')
    				->join('backup_line','thongtinkythuat.backup_line','=','backup_line.id')  	
    				->select('chinhanh.ten','chinhanh.diachi','chinhanh.lienhe','service.ten_service','sodo','ip_kh','subnet','hd','tt','mtt.sw_port_sfp','mtt.vlan','mtt.ip_mgmt','backup_line.user_id','backup_line.password','backup_line.ip_pppoe','thongtinkythuat.equipment','main_line.mtt')
    				->get();
    	}
    	else
    	{
    		$thongtin = DB::table('thongtinkythuat')
    				->where('thongtinkythuat.id',$rq->id)
    				->join('chinhanh','thongtinkythuat.cn_id','=','chinhanh.id')
    				->join('service','thongtinkythuat.service','=','service.id')
    				->join('main_line','thongtinkythuat.main_line','=','main_line.id')
    				->join('other_line', 'main_line.other_line', '=', 'other_line.id')  
    				->join('backup_line','thongtinkythuat.backup_line','=','backup_line.id')  	   				
    				->select('chinhanh.ten','chinhanh.diachi','chinhanh.lienhe','service.ten_service','sodo','ip_kh','subnet','hd','tt','other_line.ol_user_id','other_line.ol_password','other_line.ol_ip_pppoe','backup_line.user_id','backup_line.password','backup_line.ip_pppoe','thongtinkythuat.equipment','main_line.mtt')				
    				->get();
    	}   	
    	return response()->json($thongtin);
	}
}
