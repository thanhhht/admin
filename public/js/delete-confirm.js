$(function(){
    $('.btn-xoa').click(function(event){
        var link = $(this).attr('link');
        var btn_xoa = $(this);
        $.confirm({ 
            title: 'Xác nhận',
            content: 'Bạn thực sự muốn xóa!',
            boxWidth: '30%',
            type: 'red',
            useBootstrap: false,
            buttons: {
                confirm:{
                    text: 'Xóa',
                    btnClass: 'btn-red',
                    action:function () {                           
                        $.ajax({ 
                            type:"POST",
                            url: link,
                            success: function(){ 
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    type: 'green',
                                    title: 'Thông báo',
                                    content: 'Đã xóa thành công!',
                                    buttons: {
                                        confirm: {
                                            text: 'OK',
                                            btnClass: 'btn-green',
                                            action: function(){
                                                btn_xoa.closest('tr').remove();                                         
                                            }
                                        }
                                    }
                                });
                            },
                            error: function(){
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    type: 'dark',
                                    title: 'Thông báo',
                                    content: 'Đã xảy ra lỗi!',
                                    buttons: {
                                        confirm: {
                                            text: 'OK'
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                cancel:{
                    text: 'Hủy'                 
                }         
            }
        })
    })
});