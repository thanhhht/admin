<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Thongtinkythuat extends Model
{
    protected $table = "thongtinkythuat";

    public static function getTableColumns() {
        $ttkt = new Thongtinkythuat;
        return $ttkt->getConnection()->getSchemaBuilder()->getColumnListing($ttkt->getTable());
    }

    // Hàm trả về các thông tin kỹ thuật mà user có id này tạo
    public static function getOtherTTKT(){
        $ttkt = Thongtinkythuat::where('created_by',Auth::user()->id)->get();
        return $ttkt;
    }

    // Kiểm tra chi nhánh này có sử dụng dịch vụ này chưa để thêm mới không bị trùng
    public static function checkExist($rq){
        $ttkt = Thongtinkythuat::where('cn_id',$rq->chinhanh)->get();
        foreach($ttkt as $tk){
            if($tk->service == $rq->service){
                return false;
            }                         
        }
        return true;
    }   

    // Thêm mới thông tin kỹ thuật
    public static function addThongTinKyThuat($rq){
        $ttkt = new Thongtinkythuat;
        $mainline = new Mainline;
        $backupline = new Backupline;
        $mtt = new Mtt;
        $otherline = new Otherline;
    
        $cty_id = Chinhanh::find($rq->chinhanh);
        try {
            // Kiểm tra xem là mtt hay otherline
            if($rq->mtt_other == 'mtt'){
                $mtt->sw_port_sfp = $rq->sw_port_sfp;
                $mtt->vlan= $rq->vlan;
                $mtt->ip_mgmt=$rq->ip_mgmt;
                $mtt->save();
                $mainline->mtt = $mtt->id;
                $mainline->save();
            }
            elseif($rq->mtt_other == 'other'){
                $otherline->ol_user_id = $rq->ol_user_id;
                $otherline->ol_password= $rq->ol_password;
                $otherline->ol_ip_pppoe=$rq->ol_ip_pppoe;
                $otherline->save();
                $mainline->other_line = $otherline->id;
                $mainline->save();
            }
    
            $backupline->user_id = $rq->user_id;
            $backupline->password = $rq->password;
            $backupline->ip_pppoe = $rq->ip_pppoe;
            $backupline->save();
    
    
            $ttkt->cn_id = $rq->chinhanh;
            $ttkt->cty_id = $cty_id->cty_id;
            $ttkt->service = $rq->service;
            $ttkt->equipment = $rq->equipment;
            $ttkt->sodo = $rq->sodo;
            
    
            $ttkt->ip_kh = $rq->ip_kh;
            $ttkt->subnet = $rq->subnet;
            $ttkt->hd = $rq->hd;
            $ttkt->tt = $rq->tt;
            $ttkt->main_line = $mainline->id;
            $ttkt->backup_line = $backupline->id;
            $ttkt->created_by = Auth::user()->id;
            $ttkt->save();
        }
        catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    public static function editThongTinKyThuat($rq, $id){
        $ttkt = Thongtinkythuat::find($id);
        $mainline = Mainline::find($ttkt->main_line);
        $backupline = Backupline::find($ttkt->backup_line);
        $cty_id = Chinhanh::find($rq->chinhanh);

        
        if($rq->mtt_other == 'mtt'){
            $mtt = Mtt::find($mainline->mtt);
            $mtt->sw_port_sfp = $rq->sw_port_sfp;
            $mtt->vlan= $rq->vlan;
            $mtt->ip_mgmt=$rq->ip_mgmt;
            $mtt->save();
        }
        elseif($rq->mtt_other == 'other'){
            $otherline = Otherline::find($mainline->other_line);
            $otherline->ol_user_id = $rq->ol_user_id;
            $otherline->ol_password= $rq->ol_password;
            $otherline->ol_ip_pppoe=$rq->ol_ip_pppoe;
            $otherline->save();
        }

        $backupline->user_id = $rq->user_id;
        $backupline->password = $rq->password;
        $backupline->ip_pppoe = $rq->ip_pppoe;
        $backupline->save();


        $ttkt->cn_id = $rq->chinhanh;
        $ttkt->cty_id = $cty_id->cty_id;
        $ttkt->service = $rq->service;
        $ttkt->equipment = $rq->equipment;
        $ttkt->sodo = $rq->sodo;
        
        $ttkt->ip_kh = $rq->ip_kh;
        $ttkt->subnet = $rq->subnet;
        $ttkt->hd = $rq->hd;
        $ttkt->tt = $rq->tt;

        $ttkt->save();
    }

    public static function deleteThongTinKyThuat($id){
        $ttkt = Thongtinkythuat::find($id);
        $mainline = Mainline::find($ttkt->main_line);
        $backupline = Backupline::find($ttkt->backup_line);
        
        $ttkt->delete();
        $backupline->delete();
        if($mainline->mtt == null){
            $otherline = Otherline::find($mainline->other_line);
            $mainline->delete();
            $otherline->delete();            
        }
        else {
            $mtt = Mtt::find($mainline->mtt);
            $mainline->delete();
            $mtt->delete();
        }
    }
}
