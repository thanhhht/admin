<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Congty extends Model
{
    protected $table = "congty";

    public static function getTableColumns() {
        $congty = new Congty;
        return $congty->getConnection()->getSchemaBuilder()->getColumnListing($congty->getTable());
    }
       
    public static function addCongTy($rq){
        $congty = new Congty;
        $congty->ten_cty = $rq->tencongty;
        $congty->save();
    }

    public static function editCongTy($rq, $id){
        $congty = Congty::find($id);
        $congty->ten_cty = $rq->tencongty;
        $congty->save();
    }

    public static function deleteCongTy($id){
        $congty = Congty::find($id);
        $congty->delete();
    }
}
